import React from 'react';
import {
  AdaptivityProvider,
  AppRoot,

  usePlatform,
} from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';
import ProductsList from './components/GroupList/ProductGroupList.tsx';

const App: React.FC = () => {
  const platform = usePlatform();

  return (
    <AdaptivityProvider>
      <AppRoot>
        <ProductsList platform={platform}></ProductsList>
      </AppRoot>
    </AdaptivityProvider>
  );
};

export default App;
