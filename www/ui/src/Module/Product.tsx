export default interface Product {
    id: number;
    name: string;
    price: number;
    count: number;
    supplier_email: string;
}
