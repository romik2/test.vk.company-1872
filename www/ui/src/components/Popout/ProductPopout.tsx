import { 
  useAdaptivityConditionalRender,
  PopoutWrapper,
  ModalDismissButton,
  FormLayoutGroup,
  FormItem,
  Input,
  CellButton,
  FormStatus
} from "@vkontakte/vkui";
import React from 'react';
import Product from '../../Module/Product.tsx';

type StatusType = "default" | "error" | "valid" | undefined;

interface StatusFields {
    name: StatusType;
    supplier_email: StatusType;
    count: StatusType;
    price: StatusType;
}

interface ProductPopoutProps {
  onClose: () => void;
  updateProducts: () => void;
  product: Product;
}

const ProductPopout: React.FC<ProductPopoutProps> = ({ onClose, product, updateProducts }) => {
  const { sizeX } = useAdaptivityConditionalRender();
  const [formData, setFormData] = React.useState<Product>(product);
  const [messages, setMessages] = React.useState<React.ReactNode | null>(null);
  const [statusField, setStatusField] = React.useState<StatusFields>({
    name: 'default',
    supplier_email: 'default',
    count: 'default',
    price: 'default',
  });

  const handleChange = (field: string, value: string | number) => {
    setFormData({ ...formData, [field]: value });
    validate();
  };

  const validate = async () => {
      setStatusField((prevStatusField) => ({
        ...prevStatusField,
        name: !formData.name.trim() ? 'error' : 'valid'
      }));
      const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
      setStatusField((prevStatusField) => ({
        ...prevStatusField,
        supplier_email: !emailPattern.test(formData.supplier_email) ? 'error' : 'valid'
      }));
      setStatusField((prevStatusField) => ({
        ...prevStatusField,
        count: isNaN(formData.count as number) ? 'error' : 'valid'
      }));
      const pricePattern = /^\$?[\d.,]+$/;
      setStatusField((prevStatusField) => ({
        ...prevStatusField,
        price: pricePattern.test(String(formData.price)) ? 'valid' : 'error'
      }));
  }

  const handleCreateProduct = async () => {
    try {
        const formDatas = new FormData();
        formDatas.append('name', formData.name);
        formDatas.append('supplier_email', formData.supplier_email);
        formDatas.append('count', String(formData.count));
        const cleanPriceString = String(formData.price).replace(/[$,]/g, '');
        const price = parseFloat(cleanPriceString);
        formDatas.append('price', String(price));

        let reasponse = await fetch('/products/add', {
            method: 'POST',
            body: formDatas
        });
        let result = await reasponse.json();
        if (result.result) {
          onClose();
          updateProducts();
        } else {
          setMessages(<FormStatus header="Ошибка" mode="error">{result.message}</FormStatus>);
        }
    } catch (error) {
        console.error('Ошибка при создании продукта:', error);
    }
  };

  const handleUpdateProduct = async () => {
    try {
        const formDatas = new FormData();
        formDatas.append('name', formData.name);
        formDatas.append('supplier_email', formData.supplier_email);
        formDatas.append('count', String(formData.count));
        const cleanPriceString = String(formData.price).replace(/[$,]/g, '');
        const price = parseFloat(cleanPriceString);
        formDatas.append('price', String(price));

        let reasponse = await fetch(`/products/edit?id=${formData.id}`, {
          method: 'POST',
          body: formDatas
        });
        let result = await reasponse.json();
        if (result.result) {
          onClose();
          updateProducts();
        } else {
          setMessages(<FormStatus header="Ошибка" mode="error">{result.message}</FormStatus>);
        }
    } catch (error) {
        console.error('Ошибка при создании продукта:', error);
    }
  };

  return (
    <PopoutWrapper onClick={onClose}>
      <div
        style={{
          backgroundColor: 'var(--vkui--color_background_content)',
          borderRadius: 8,
          position: 'relative',
          padding: '12px',
        }}
      >
        {messages}

        <FormLayoutGroup mode="vertical">
          <FormItem htmlFor="name" top="Наименование" status={statusField.name}>
            <Input id="name" value={formData.name} onChange={(e) => handleChange('name', e.target.value)} />
          </FormItem>
          <FormItem htmlFor="supplier_email" top="Почта контрагента" status={statusField.supplier_email}>
            <Input type="email" id="supplier_email" value={formData.supplier_email} onChange={(e) => handleChange('supplier_email', e.target.value)} />
          </FormItem>
          <FormItem htmlFor="count" top="Количество" status={statusField.count}>
            <Input id="count" value={formData.count} onChange={(e) => handleChange('count', e.target.value)} />
          </FormItem>
          <FormItem htmlFor="price" top="Цена" status={statusField.price}>
            <Input id="price" value={formData.price} onChange={(e) => handleChange('price', e.target.value)} />
          </FormItem>
          <FormItem>
            <CellButton onClick={product.id ? handleUpdateProduct : handleCreateProduct} centered>{product.id ? "Обновить" : "Добавить"}</CellButton>
          </FormItem>
        </FormLayoutGroup>

        {sizeX.regular && (
          <ModalDismissButton className={sizeX.regular.className} onClick={onClose}>
            Закрыть кастомное модальное окно
          </ModalDismissButton>
        )}
      </div>
    </PopoutWrapper>
  );
};

export default ProductPopout;
