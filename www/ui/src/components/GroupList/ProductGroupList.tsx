import React, { useEffect, useState } from 'react';
import { Header, Group, SimpleCell, CellButton, FormLayoutGroup, FormItem, Counter, Input, AdaptivityProvider, 
    SplitCol,
    SplitLayout,
    View,
    Panel,
    PanelHeader, 
    PlatformType} from '@vkontakte/vkui';
import ProductPopout from '../Popout/ProductPopout.tsx';
import Product from '../../Module/Product.tsx';

interface ProductsListProps {
    platform: PlatformType;
}

const ProductsList: React.FC<ProductsListProps> = ({ platform }) => {
    const [products, setProducts] = useState<Product[]>([]);
    const [filteredProducts, setFilteredProducts] = useState<Product[]>([]);
    const [sortOrder, setSortOrder] = useState<'asc' | 'desc' | null>(null);
    const [currentSortKey, setCurrentSortKey] = useState<'name' | 'price' | null>(null);
    const [popout, setPopout] = React.useState<React.ReactNode | null>(null);

    const formOpen = (data) => {
        setPopout(<ProductPopout product={data} updateProducts={updateProducts} onClose={() => setPopout(null)} />);
    };

    const updateProducts = async () => {
        try {
            const response = await fetch('/products/getAll');
            const data = await response.json();
            setProducts(data.products);
            setFilteredProducts(data.products);
        } catch (error) {
            console.error('Ошибка при обработке продуктов:', error);
        }
    };
    
    useEffect(() => {
        updateProducts();
    }, []);

    const formatPrice = (price: number) => {
        return new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(price);
    };

    const deleteProduct = async (productId: number) => {
        try {
            await fetch(`/products/delete?id=${productId}`, {
                method: 'DELETE'
            });
            let updatedProducts = products.filter(product => product.id !== productId);
            setProducts(updatedProducts);
            updatedProducts = filteredProducts.filter(product => product.id !== productId);
            setFilteredProducts(updatedProducts);
        } catch (error) {
            console.error('Ошибка при удалении продукта:', error);
        }
    };

    const handleSearch = (searchValue: string) => {
        const filtered = products.filter(product => product.name.toLowerCase().includes(searchValue.toLowerCase()));
        setFilteredProducts(filtered);
    };

    const handleSort = (sortKey: 'name' | 'price') => {
        let newSortOrder: 'asc' | 'desc';

        if (sortOrder === null) {
            newSortOrder = 'asc';
        } else if (sortOrder === 'asc' && sortKey === currentSortKey) {
            newSortOrder = 'desc';
        } else {
            newSortOrder = 'asc';
        }

        const sortedProducts = [...filteredProducts].sort((a, b) => {
            if (sortKey === 'name') {
                return newSortOrder === 'asc' ? a.name.localeCompare(b.name) : b.name.localeCompare(a.name);
            } else {
                return newSortOrder === 'asc' ? a.price - b.price : b.price - a.price;
            }
        });

        setSortOrder(newSortOrder);
        setCurrentSortKey(sortKey);
        setFilteredProducts(sortedProducts);
    };

    return (
        <SplitLayout header={platform !== 'vkcom' && <PanelHeader delimiter="none" />} popout={popout}>
            <SplitCol animate={false}>
                <View activePanel="main">
                <Panel id="main">
                    <PanelHeader>test.vk.company-1872</PanelHeader>
                    <AdaptivityProvider sizeY='regular'>
                        <Group>
                            <FormLayoutGroup mode="horizontal">
                                <FormItem top="Фильтр по подстроке имени товара" htmlFor="search">
                                    <Input id="search" onChange={(e) => handleSearch(e.target.value)} />
                                </FormItem>
                                <FormItem>
                                    <CellButton centered onClick={() => formOpen({
                                        name: '',
                                        supplier_email: '',
                                        count: 0,
                                        price: "$0.00"
                                        })}>Добавить продукт</CellButton>
                                </FormItem>
                            </FormLayoutGroup>
                        </Group>

                        <Group>
                            <FormLayoutGroup mode="horizontal">
                                <FormItem>
                                    <Group header={<Header mode="secondary" aside={
                                        <CellButton onClick={() => handleSort('name')}>
                                            {sortOrder && currentSortKey === 'name' ? (sortOrder === 'asc' ? '⬇️' : '⬆️') : '↕️'}
                                        </CellButton>
                                    }>Наименование</Header>}>
                                        {filteredProducts.map((product) => (
                                            <FormLayoutGroup key={product.id} mode="horizontal">
                                                <FormItem>
                                                    <SimpleCell indicator={
                                                        <Counter size="s" mode="prominent">
                                                            {product.count}
                                                        </Counter>
                                                    }>
                                                        {product.name}
                                                    </SimpleCell>
                                                </FormItem>
                                            </FormLayoutGroup>
                                        ))}
                                    </Group>
                                </FormItem>
                                <FormItem>
                                    <Group header={<Header mode="secondary" aside={
                                        <CellButton onClick={() => handleSort('price')}>
                                            {sortOrder && currentSortKey === 'price' ? (sortOrder === 'asc' ? '⬇️' : '⬆️') : '↕️'}
                                        </CellButton>
                                    }>Цена</Header>}>
                                        {filteredProducts.map((product) => (
                                            <FormLayoutGroup key={product.id} mode="horizontal">
                                                <FormItem>
                                                    <SimpleCell>{formatPrice(product.price)}</SimpleCell>
                                                </FormItem>
                                            </FormLayoutGroup>
                                        ))}
                                    </Group>
                                </FormItem>
                                <FormItem>
                                <Group header={<Header mode="secondary" aside={
                                    <CellButton>
                                    </CellButton>
                                }>Действия</Header>}>
                                        {filteredProducts.map((product) => (
                                            <FormLayoutGroup key={product.id} mode="horizontal">
                                                <FormItem>
                                                    <CellButton centered onClick={() => formOpen({
                                                        id: product.id,
                                                        name: product.name,
                                                        supplier_email: product.supplier_email,
                                                        count: product.count,
                                                        price: formatPrice(product.price)
                                                        })}>Редактировать продукт</CellButton>
                                                </FormItem>
                                                <FormItem>
                                                    <CellButton centered onClick={() => deleteProduct(product.id)} mode="danger">Удалить продукт</CellButton>
                                                </FormItem>
                                            </FormLayoutGroup>
                                        ))}
                                    </Group>
                                </FormItem>
                            </FormLayoutGroup>
                        </Group>
                    </AdaptivityProvider>
                    </Panel>
                </View>
            </SplitCol>
        </SplitLayout>
    );
};

export default ProductsList;
