<?php

namespace Task\Validate;


class ProductValidate {
    public static function validate($formData)
    {
        $statusField = [];

        if (empty(trim($formData['name']))) {
            $statusField[] = 'Наименование (Пример: Продукт 1)';
        }
    
        $emailPattern = "/^[^\s@]+@[^\s@]+\.[^\s@]+$/";
        if (!preg_match($emailPattern, $formData['supplier_email'])) {
            $statusField[] = 'Почта контрагента (Пример: sales@romtuck.ru)';
        }
    
        if (!is_numeric($formData['count'])) {
            $statusField[] = 'Количество (Введите число)';
        }

        return $statusField;
    }
}
