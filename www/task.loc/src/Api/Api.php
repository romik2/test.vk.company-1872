<?php

namespace Task\Api;

use Task\Api\ProductApi;

class Api {

    private static $routes = [
        '/products/getAll' => ['class' => ProductApi::class, 'func' => 'list', 'method' => 'GET'],
        '/products/get' => ['class' => ProductApi::class, 'func' => 'detail', 'method' => 'GET'],
        '/products/add' => ['class' => ProductApi::class, 'func' => 'add', 'method' => 'POST'],
        '/products/edit' => ['class' => ProductApi::class, 'func' => 'edit', 'method' => 'POST'],
        '/products/delete' => ['class' => ProductApi::class, 'func' => 'delete', 'method' => 'DELETE'],
    ];

    public static function init(): void {
        $routes = self::$routes;
        $requestedRoute = strtok($_SERVER['REQUEST_URI'], '?');

        if (!empty($routes[$requestedRoute])) {
            $route = $routes[$requestedRoute];

            if ($_SERVER['REQUEST_METHOD'] === $route['method']) {
                $class = $route['class'];
                $func = $route['func'];
                $result = $class::$func();
                echo json_encode($result);
            } else {
                echo json_encode(['result' => false, 'message' => 'Method not allowed for this route']);
            }
        } else {
            echo json_encode(['result' => false, 'message' => 'Route not found']);
        }
    }
}
