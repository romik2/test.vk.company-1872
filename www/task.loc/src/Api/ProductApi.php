<?php

namespace Task\Api;

use Task\Model\Product;
use Task\Validate\ProductValidate;

class ProductApi {

    public static function list()
    {
        return ['result' => true, 'products' => Product::findAll()];
    }

    public static function detail()
    {
        return ['result' => true, 'products' => Product::findBy($_GET)];
    }

    public static function add()
    {
        $formData = $_POST;
        $statusField = ProductValidate::validate($formData);
        $field = implode(', ', $statusField);
        if (!empty($statusField)) {
            return ['result' => false, 'message' => "Ошибка проверки полей ($field)"];
        }
        return ['result' => true, 'products' => Product::create($_POST)];
    }

    public static function edit()
    {
        $formData = $_POST;
        $statusField = ProductValidate::validate($formData);
        $field = implode(', ', $statusField);
        if (!empty($statusField)) {
            return ['result' => false, 'message' => "Ошибка проверки полей ($field)"];
        }
        return ['result' => true, 'products' => Product::update($_POST, $_GET)];
    }

    public static function delete()
    {
        return ['result' => true, 'products' => Product::delete($_GET)];
    }
}
