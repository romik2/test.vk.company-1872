<?php

namespace Task\Model;

use Task\Connection\MySqlConnection;

use ReflectionClass;

class Product {

    public static function findAll()
    {
        return MySqlConnection::select(['id', 'name', 'supplier_email', 'count', 'price'], 'product');
    }

    public static function findBy($where = [], $orderBy = [], $limit = 10000)
    {
        return MySqlConnection::select(['id', 'name', 'supplier_email', 'count', 'price'], 'product', $where, $orderBy, $limit);
    }

    public static function delete($where = [])
    {
        return MySqlConnection::delete('product', $where);
    }

    public static function create($data = [])
    {
        return MySqlConnection::insert('product', $data);
    }

    public static function update($data = [], $where = [])
    {
        return MySqlConnection::update('product', $data, $where);
    }
}
