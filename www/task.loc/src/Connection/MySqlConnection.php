<?php

namespace Task\Connection;

use mysqli;

class MySqlConnection {

    public static function connection()
    {
        $conn = new mysqli('mysql', 'admin', 'secret', 'task');

        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        return $conn;
    }

    public static function select($fields, $table, $where = [], $orderBy = [], $limit = 10000)
    {
        $conn = self::connection();
        
        $sql = "SELECT " . implode(', ', $fields) . " FROM $table";
        $params = [];
        
        if (!empty($where)) {
            $sql .= " WHERE ";
            $whereClauses = [];
            foreach ($where as $key => $value) {
                $whereClauses[] = "$key = ?";
                $params[] = $value;
            }
            $sql .= implode(' AND ', $whereClauses);
        }
        
        if (!empty($orderBy)) {
            $sql .= " ORDER BY " . implode(', ', $orderBy);
        }
        
        $sql .= " LIMIT ?";
        $params[] = $limit;
    
        $stmt = $conn->prepare($sql);
        
        if (!empty($params)) {
            $paramTypes = str_repeat('s', count($params));
            $stmt->bind_param($paramTypes, ...$params);
        }
    
        $stmt->execute();
        
        $result = $stmt->get_result();
        
        $data = [];
        while ($row = $result->fetch_assoc()) {
            $data[] = $row;
        }    
    
        $stmt->close();
        $conn->close();
    
        return $data;
    }
    
    public static function insert($table, $data)
    {
        $conn = self::connection();
        
        $fields = implode(', ', array_keys($data));
        $values = array_values($data);
        $placeholders = implode(', ', array_fill(0, count($values), '?'));
    
        $sql = "INSERT INTO $table ($fields) VALUES ($placeholders)";
    
        $stmt = $conn->prepare($sql);
    
        $paramTypes = str_repeat('s', count($values));
        if ($paramTypes !== '') {
            $stmt->bind_param($paramTypes, ...$values);
        }
    
        $stmt->execute();
    
        $stmt->close();
        $conn->close();
    }

    public static function delete($table, $where)
    {
        $conn = self::connection();

        $sql = "DELETE FROM $table WHERE ";
        $whereClauses = [];
        $params = [];
        foreach ($where as $key => $value) {
            $whereClauses[] = "$key = ?";
            $params[] = $value;
        }
        $sql .= implode(' AND ', $whereClauses);

        $stmt = $conn->prepare($sql);

        $paramTypes = str_repeat('s', count($params));
        $stmt->bind_param($paramTypes, ...$params);

        $stmt->execute();

        $stmt->close();
        $conn->close();
    }

    public static function update($table, $data, $where)
    {
        $conn = self::connection();

        $setClauses = [];
        $params = [];
        foreach ($data as $key => $value) {
            $setClauses[] = "$key = ?";
            $params[] = $value;
        }

        $sql = "UPDATE $table SET " . implode(', ', $setClauses) . " WHERE ";
        $whereClauses = [];
        foreach ($where as $key => $value) {
            $whereClauses[] = "$key = ?";
            $params[] = $value;
        }
        $sql .= implode(' AND ', $whereClauses);

        $stmt = $conn->prepare($sql);

        $paramTypes = str_repeat('s', count($params));
        $stmt->bind_param($paramTypes, ...$params);

        $stmt->execute();

        $stmt->close();
        $conn->close();
    }

}