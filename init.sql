CREATE DATABASE IF NOT EXISTS task;
USE task;
CREATE TABLE IF NOT EXISTS product (
    id INT auto_increment,
    name VARCHAR(255),
    supplier_email VARCHAR(255),
    count INT,
    price DECIMAL(65,2),
    primary key (id)
);